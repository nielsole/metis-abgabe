from typing import List

from pathlib import Path

import yaml
from slugify import slugify

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from visualization.graphs import __create_heatmap, __create_graph
from visualization.utils import experiment_table


def create_graphs(experiments: List, sla_names: List[str], artifacts_directory: str = "./artifacts"):
    base_dir = Path(artifacts_directory)
    for sla_name in sla_names:
        print("Creating artifacts for SLA: {}".format(sla_name))
        memory, cpu, table = experiment_table(experiments)

        def extract_metric(experiment) -> float:
            try:
                return experiment["sla"][sla_name]["value"]
            except KeyError:
                return -1
        new_table = np.vectorize(extract_metric)(table)
        sla_dir = base_dir / slugify(sla_name)
        sla_dir.mkdir(exist_ok=True, parents=True)
        try:
            if "50" in sla_name:
                fancy_sla_name = "latency in the 50th percentile (ms)"
            if "99" in sla_name:
                fancy_sla_name = "latency in the 99th percentile (ms)"
            __create_graph(memory, cpu, new_table, experiments, sla_name, sla_dir, fancy_sla_name=fancy_sla_name)
            __create_heatmap(memory, cpu, new_table, experiments, sla_name, sla_dir, fancy_sla_name=fancy_sla_name)
        except Exception as e:
            print(e)
            print("There was an error creating the graphs for SLA {}".format(sla_name))
    pass


if __name__ == '__main__':
    with open("artifacts/summary.yaml") as f:
        artifact = yaml.safe_load(f)
        sla_names = set()
        for experiment in artifact["experiments"]:
            for key in experiment["sla"].keys():
                sla_names.add(key)
        create_graphs(artifact["experiments"], list(sla_names), artifacts_directory="visualizations")
