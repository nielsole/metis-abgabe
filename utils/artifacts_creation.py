import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import axes3d
from tkinter import *
from slugify import slugify
import numpy as np
from typing import List, Dict, IO, Callable

import yaml

from experiment_controller.experiment import Experiment
from pathlib import Path


def write_experiment_result_yaml(experiment: Experiment, artifacts_directory: str = "./artifacts"):
    artifacts_dir = Path(artifacts_directory)
    current_dir = (artifacts_dir / experiment.prefix)
    current_dir.mkdir(parents=True)
    for category, nested_log in experiment.result.logs.items():
        for container_name, log in nested_log.items():
            if log is None:
                continue
            folder = (current_dir / 'logs' / category / container_name)
            folder.mkdir(parents=True)
            with (folder / container_name).open('w') as log_file:
                log_file.write(log)
    nice_result_obj = dict()
    nice_result_obj["sla"] = experiment.result.slas
    nice_result_obj["resources"] = experiment.resources
    nice_result_obj["oom"] = experiment.result.oom
    with (current_dir / 'info.yaml').open('w') as info_file:
        yaml.dump(nice_result_obj, info_file)
    # create folder for eperiment
    # create yaml with information esp. resources and SLAs
    # create subfolder for logs per container
    # write logs to files


def _sort_cpu(experiments: List[Experiment]) -> List[Experiment]:
    """
    Returns sorted list of experiments by their CPU allocation in ascending order
    :param experiments:
    :return:
    """
    def get_mem(experiment: Experiment):
        return experiment.resources["cpu"]

    experiments = experiments[:]
    experiments.sort(key=get_mem)
    return experiments


def _sort_memory(experiments: List[Experiment]):
    """
    Returns sorted list of experiments by their memory allocation in ascending order.
    :param experiments:
    :return:
    """
    def get_mem(experiment: Experiment):
        return experiment.resources["memory"]

    experiments = experiments[:]
    experiments.sort(key=get_mem)
    return experiments


def _html_page_header(experiments: List[Experiment]) -> (str, dict):
    """
    Generates the header row for a table of memory and cpu allocations
    # ------------- cpu
    # |
    # |
    # |
    # |
    # memory
    :param experiments:
    :return: string of HTML table header, dict of [cpu allocation -> offset in a table row]
    """
    ret = "<tr><th>memory \\ cpu</th>"
    previous_cpu = -1
    headers: Dict[int, int] = dict()
    i = 0
    for experiment in experiments:
        if experiment.resources["cpu"] == previous_cpu:
            continue
        headers[experiment.resources["cpu"]] = i
        i += 1
        previous_cpu = experiment.resources["cpu"]
        ret += "<th>{}</th>".format(round(experiment.resources["cpu"], 3))
    ret += "</tr>"
    return ret, headers


def __default_success_field_value_getter(experiment) -> str:
    if experiment.result.oom:
        return "OOM"
    if experiment.result.failed:
        return "ERROR"
    if experiment.result.sla_met() and not experiment.result.oom:
        return "Success"
    return "Failure"


def __create_throughput_per_resource_getter(factors) -> Callable[[Experiment], str]:
    def func(experiment: Experiment) -> str:
        if experiment.result.failed or experiment.result.oom or not experiment.result.sla_met():
            return "No value"
        return str(experiment.result.throughput * (
                   factors["cpu"] * experiment.resources["cpu"] + factors["memory"] * experiment.resources["memory"]))

    return func


def _html_page_line(experiments: List[Experiment], headers: dict,
                    field_value_getter: Callable[[Experiment], str] = __default_success_field_value_getter) -> str:
    memory = experiments[0].resources["memory"]
    ret = "<tr><td>{}</td>".format(memory)
    line = [None] * len(headers.keys())
    for experiment in experiments:
        if experiment.resources["memory"] != memory:
            continue
        line[headers[experiment.resources["cpu"]]] = field_value_getter(experiment)

    for item in line:
        ret += "<td>{}</td>".format(item)
    return ret + "</tr>"


def experiment_table(experiments: List[Experiment])-> (List[float], List[float], object):
    """
    outputs the experiments in a 2d array containing experiments in a grid according to their resources
    :param experiments:
    :return: memory, cpu, experiments2dArray
    """
    experiments_output = []
    experiments_with_same_memory: List[Experiment] = list()
    previous_memory = -1
    memory_output = list()
    for experiment in _sort_memory(experiments):
        if experiment.resources["memory"] not in memory_output:
            memory_output.append(experiment.resources["memory"])
    cpu_output = list()
    for i, experiment in enumerate(_sort_memory(experiments)):
        if experiment.resources["memory"] != previous_memory:
            if len(experiments_with_same_memory) > 0:
                experiments_output.append(_sort_cpu(experiments_with_same_memory))
            experiments_with_same_memory = [experiment]
        else:
            experiments_with_same_memory.append(experiment)
        cpu_output = list()
        for experiment2 in _sort_cpu(experiments_with_same_memory):
            cpu_output.append(experiment2.resources["cpu"])
        previous_memory = experiment.resources["memory"]
    experiments_output.append(_sort_cpu(experiments_with_same_memory))

    return memory_output, cpu_output, np.asarray(experiments_output)


def _create_table(stream: IO, experiments: List[Experiment], value_getter: Callable[[Experiment], str] = None):
    """
    Creates a table of CPU and memory
    :param stream: HTML output stream
    :param experiments: list of experiments to be outputted
    :param value_getter:
    :return:
    """
    stream.write("<table>")
    header_line, headers = _html_page_header(_sort_cpu(experiments))
    stream.write(header_line)
    memories, cpus, table = experiment_table(experiments)
    for row in table:
        if value_getter is not None:
            stream.write(_html_page_line(row, headers,
                                         field_value_getter=value_getter))
        else:
            stream.write(_html_page_line(row, headers))
    stream.write("</table>")


def create_html_page(experiments: List[Experiment], artifacts_directory: str = "./artifacts"):
    """
    Creates an output HTML page containing a summary table with an overview of the experiments and writes it to summary.html in the given directory
    :param experiments: list of experiments that can be laid in a grid
    :param artifacts_directory: directory in which to create summary.html
    :return:
    """
    artifacts_dir = Path(artifacts_directory)
    with (artifacts_dir / "summary.html").open("w") as result_file:
        result_file.write("<html><head></head><body>")
        _create_table(result_file, experiments)
        result_file.write("</body></html>")


def write_yaml_summary(experiments: List[Experiment], artifacts_directory: str = "./artifacts"):
    """
    Creates an output YAML summary.yaml containing a summary of all experiments into the given directory
    :param experiments:
    :param artifacts_directory: directory in which to write saml summary
    :return:
    """
    artifacts_dir = Path(artifacts_directory)
    experiment_list: List[dict] = list()
    summary: Dict = dict()
    for experiment in experiments:
        experiment_summary = dict()
        experiment_summary["resources"] = experiment.resources
        experiment_summary["prefix"] = experiment.prefix
        experiment_summary["sla_met"] = experiment.result.sla_met()
        experiment_summary["sla"] = experiment.result.slas
        experiment_summary["oom"] = experiment.result.oom
        experiment_summary["failed"] = experiment.result.failed
        experiment_list.append(experiment_summary)
    summary["experiments"] = experiment_list
    summary["successful_experiments"] = list(
        filter(lambda experiment: not experiment["oom"] and experiment["sla_met"] and not experiment["failed"],
               experiment_list))

    with (artifacts_dir / "summary.yaml").open("w") as result_file:
        yaml.dump(summary, result_file, default_flow_style=False)
