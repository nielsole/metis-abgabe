import unittest

import io
import numpy as np
from utils.artifacts_creation import _html_page_header, experiment_table, _create_table


class HtmlPageTestCase(unittest.TestCase):

    def test__html_page_header(self):
        experiment = type('test', (object,), {})()
        experiment.resources = {"memory": 50, "cpu": 1}
        self.assertEqual("<tr><th>memory \\ cpu</th><th>1</th></tr>", _html_page_header([experiment])[0])
        self.assertEqual({1: 0}, _html_page_header([experiment])[1])
        self.assertEqual(True, True)

    def test_experiment_table_single(self):
        experiment = type('test', (object,), {})()
        experiment.resources = {"memory": 50, "cpu": 1}
        memory, cpu, table = experiment_table([experiment])
        self.assertEqual([50], memory)
        self.assertEqual([1], cpu)
        ar = [[experiment]]
        self.assertTrue(len(table) == 1)
        self.assertEqual(ar[0], table[0])

    def test_experiment_table_multiple_cpu(self):
        experiments = list()
        for i in range(5):
            experiment = type('test', (object,), {})()
            experiment.resources = {"memory": 50, "cpu": i}
            experiments.append(experiment)
        memory, cpu, table = experiment_table(experiments)
        self.assertEqual(1, len(memory))
        self.assertEqual(5, len(cpu))
        self.assertEqual(1, len(table))
        self.assertEqual(5, len(table[0]))
        self.assertEqual(experiments, list(table[0]))
        # Check if reversing works
        memory, cpu, table = experiment_table(experiments[::-1])
        self.assertEqual(experiments, list(table[0]))

    def test_experiment_table_multiple_memory(self):
        experiments = list()
        for i in range(100, 600, 100):
            experiment = type('test', (object,), {})()
            experiment.resources = {"memory": i, "cpu": 1}
            experiments.append(experiment)
        memory, cpu, table = experiment_table(experiments)
        self.assertEqual(5, len(memory))
        self.assertEqual(1, len(cpu))
        self.assertEqual(5, len(table))
        self.assertEqual(1, len(table[0]))
        self.assertIn(table[0][0], experiments)

    def test__create_table(self):
        experiments = list()
        for i in range(100, 600, 100):
            for j in range(1, 3):
                experiment = type('test', (object,), {})()
                experiment.result = type('test', (object,), {})()
                experiment.result.oom = False
                experiment.result.failed = False
                experiment.result.sla_met = lambda: True
                experiment.resources = {"memory": i, "cpu": j}
                experiments.append(experiment)
        # Self check 2x5 experiments
        self.assertEqual(10, len(experiments))
        stream = io.StringIO("")
        _create_table(stream, experiments)
        stream.seek(0)
        table_output = stream.read()
        self.assertEqual(330, len(table_output))
        self.assertEqual(6, table_output.count("<tr>"))
        self.assertEqual(6, table_output.count("</tr>"))
