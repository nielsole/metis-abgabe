def parse_cpu(value: str):
    """
    Transforms CPU values used in Kubernetes yamls into number of CPU cores
    :param value:
    :return:
    """
    try:
        return float(value)
    except ValueError:
        if value[-1] == "m":
            return float(value[:-1]) / 1000


def parse_memory(value: str):
    """
    Transforms Memory values used in Kubernetes yamls into number of bytes of memory
    :param value:
    :return:
    """
    lookup = {
        "K": 10 ** 3,
        "M": 10 ** 6,
        "G": 10 ** 9,
        "Ki": 2 ** 10,
        "Mi": 2 ** 20,
        "Gi": 2 ** 30,
    }
    try:
        return int(value)
    except ValueError:
        try:
            return int(value[:-1]) * lookup[value[-1]]
        except ValueError:
            return int(value[:-2]) * lookup[value[-2:]]
