import yaml


def parse_config(path: str) -> dict:
    with open(path, 'r') as stream:
        config = yaml.safe_load(stream)
        return config


def resource_in_range(resources, minimum, maximum) -> bool:
    """
    Determines if a proposed resource configuration is between two bounds
    :param resources:
    :param minimum:
    :param maximum:
    :return:
    """
    bigger_than_minimum = resources["cpu"] >= minimum["cpu"] and resources["memory"] >= minimum["memory"]
    smaller_than_maximum = resources["cpu"] <= maximum["cpu"] and resources["memory"] <= maximum["memory"]
    return bigger_than_minimum and smaller_than_maximum
