import unittest

from utils.kubernetes_types import parse_cpu, parse_memory


class KubernetesTypesTestCase(unittest.TestCase):
    def test_cpu(self):
        scenarios = [
            (1, 1),
            ("1", 1),
            (".1", .1),
            ("100m", .1),
            ("1000m", 1),
        ]
        for scenario in scenarios:
            cpu = parse_cpu(scenario[0])
            self.assertEqual(scenario[1], cpu)

    def test_memory(self):
        scenarios = [
            (1, 1),
            ("1", 1),
            ("1K", 1000),
            ("1M", 1000000),
            ("1G", 1000000000),
            ("1Ki", 1024),
            ("1Mi", 1024 ** 2),
            ("1Gi", 1024 ** 3),
        ]
        for scenario in scenarios:
            memory = parse_memory(scenario[0])
            self.assertEqual(scenario[1], memory)


if __name__ == '__main__':
    unittest.main()
