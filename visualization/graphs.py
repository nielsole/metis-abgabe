import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import axes3d
from tkinter import *


def __create_graph(cpu, memory, values, experiments, sla_name, path=None, fancy_sla_name=None):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # Create data for wireframe
    X = np.zeros((len(cpu), len(memory)))
    for i, _ in enumerate(memory):
        X[:, i] = cpu
    Y = np.zeros((len(cpu), len(memory)))
    for i, _ in enumerate(cpu):
        Y[i, :] = memory

    # Create data for scatter
    scatter_X = list()
    scatter_Y = list()
    scatter_Z = list()
    scatter_color = list()
    for experiment in experiments:
        scatter_X.append(int(experiment["resources"]["memory"] / 1000000))
        scatter_Y.append(experiment["resources"]["cpu"])
        if not experiment["failed"] and not experiment["oom"]:
            scatter_Z.append(experiment["sla"][sla_name]["value"] / 1000)
            if experiment["sla_met"]:
                scatter_color.append("green")
            elif experiment["sla"][sla_name]["met"]:
                scatter_color.append("yellow")
            else:
                scatter_color.append("red")

        else:
            scatter_Z.append(0)
            scatter_color.append("black")

    # Display the data
    ax.plot_wireframe(X / 1000000, Y, values / 1000)
    ax.scatter(scatter_X, scatter_Y, zs=scatter_Z, color=scatter_color, alpha=1.0)
    ax.set_xlabel("Memory(MB)")
    ax.set_ylabel("CPU(cores)")
    if fancy_sla_name is not None:
        ax.set_zlabel(fancy_sla_name)
    else:
        ax.set_zlabel(sla_name)
    if path:
        # for i in range(0, 360, 45):
        ax.view_init(20, 45)  # i)
        plt.draw()
        plt.savefig(Path(path) / ("graph" + str(45) + ".png"), dpi=400)
    else:
        ax.view_init(30, 45)
        plt.show()


def __create_heatmap(memory, cpu, values, experiments, sla_name: str, path=None, fancy_sla_name=None):
    fig, ax = plt.subplots()
    im = ax.imshow(values)

    # Adapted from: https://matplotlib.org/gallery/images_contours_and_fields/image_annotated_heatmap.html
    # We want to show all ticks...
    ax.set_xticks(np.arange(len(cpu)))
    ax.set_yticks(np.arange(len(memory)))
    # ... and label them with the respective list entries
    ax.set_xticklabels(np.round(np.asarray(cpu), 2))
    ax.set_yticklabels(np.round(np.asarray(memory) / 1000000, 0))

    ax.set_xlabel("CPU(cores)")
    ax.set_ylabel("Memory(MB)")

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    result_latency = "result.latency"
    if str.startswith(sla_name, result_latency):
        values = values / 1000
    # Loop over data dimensions and create text annotations.
    for i in range(len(memory)):
        for j in range(len(cpu)):
            # TODO Add a more generic formatting approach
            text = ax.text(j, i, int(values[i, j]),
                           ha="center", va="center", color="w")
    if fancy_sla_name is not None:
        ax.set_title("{}".format(fancy_sla_name))
    else:
        ax.set_title("Metric: {}(ms)".format(sla_name))
    fig.tight_layout()
    if path:
        plt.draw()
        plt.savefig(Path(path) / ("heatmap.png"), dpi=400)
    else:
        plt.show()
