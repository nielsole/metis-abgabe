import unittest

import yaml

from visualization.utils import experiment_table


class ExperimentTableTestCase(unittest.TestCase):

    def test_experiment_table_single(self):
        experiment = dict()
        experiment["resources"] = {"memory": 50, "cpu": 1}
        memory, cpu, table = experiment_table([experiment])
        self.assertEqual([50], memory)
        self.assertEqual([1], cpu)
        ar = [[experiment]]
        self.assertTrue(len(table) == 1)
        self.assertEqual(ar[0], table[0])

    def test_experiment_table_multiple_cpu(self):
        experiments = list()
        for i in range(5):
            experiment = dict()
            experiment["resources"] = {"memory": 50, "cpu": i}
            experiments.append(experiment)
        memory, cpu, table = experiment_table(experiments)
        self.assertEqual(1, len(memory))
        self.assertEqual(5, len(cpu))
        self.assertEqual(1, len(table))
        self.assertEqual(5, len(table[0]))
        self.assertEqual(experiments, list(table[0]))
        # Check if reversing works
        memory, cpu, table = experiment_table(experiments[::-1])
        self.assertEqual(experiments, list(table[0]))

    def test_experiment_table_multiple_memory(self):
        experiments = list()
        for i in range(100, 600, 100):
            experiment = dict()
            experiment["resources"] = {"memory": i, "cpu": 1}
            experiments.append(experiment)
        memory, cpu, table = experiment_table(experiments)
        self.assertEqual(5, len(memory))
        self.assertEqual(1, len(cpu))
        self.assertEqual(5, len(table))
        self.assertEqual(1, len(table[0]))
        self.assertIn(table[0][0], experiments)

    def test_from_file(self):
        with open("./visualization/test_files/summary.yaml") as f:
            results = yaml.safe_load(f)
            experiment_table(results["experiments"])


if __name__ == '__main__':
    unittest.main()
