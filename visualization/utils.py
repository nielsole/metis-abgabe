from typing import List
import numpy as np


def _sort_memory(experiments: List):
    def get_mem(experiment):
        return experiment["resources"]["memory"]

    experiments = experiments[:]
    experiments.sort(key=get_mem)
    return experiments


def _sort_cpu(experiments: List):
    def get_mem(experiment):
        return experiment["resources"]["cpu"]

    experiments = experiments[:]
    experiments.sort(key=get_mem)
    return experiments


def experiment_table(experiments: List)-> (List[float], List[float], object):
    """

    :param experiments:
    :return: memory, cpu, experiments2dArray
    """
    experiments_output = []
    experiments_with_same_memory: List = list()
    previous_memory = -1
    memory_output = list()
    for experiment in _sort_memory(experiments):
        if experiment["resources"]["memory"] not in memory_output:
            memory_output.append(experiment["resources"]["memory"])
    cpu_output = list()
    for i, experiment in enumerate(_sort_memory(experiments)):
        if experiment["resources"]["memory"] != previous_memory:
            if len(experiments_with_same_memory) > 0:
                experiments_output.append(_sort_cpu(experiments_with_same_memory))
            experiments_with_same_memory = [experiment]
        else:
            experiments_with_same_memory.append(experiment)
        cpu_output = list()
        for experiment2 in _sort_cpu(experiments_with_same_memory):
            cpu_output.append(experiment2["resources"]["cpu"])
        previous_memory = experiment["resources"]["memory"]
    experiments_output.append(_sort_cpu(experiments_with_same_memory))

    return memory_output, cpu_output, np.asarray(experiments_output)
