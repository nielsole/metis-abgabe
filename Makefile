
test:
	python3 -m unittest
	pycodestyle .  --ignore E501,E241 --exclude .venv

clean:
	rm -R artifacts

init:
	source .venv/bin/activate
