FROM eu.gcr.io/kubernetes-berlin/metis-base
ADD . /app
WORKDIR /app
RUN pip3 install -r requirements.txt