from core_controller.grid_strategy import GridStrategy

"""
The strategies are responsible for the entire lifecycle of the experimentation phase
* determining which experiments should be run
* scheduling experiments
* triggering metrics collection
"""

available_strategies = {
    'grid_strategy': GridStrategy,
}
