from typing import List

from experiment_controller.experiment_result import ExperimentResult
from utils.kubernetes_types import parse_cpu, parse_memory


class AbstractStrategy(object):
    """
    All strategies accept the following options:

    'resources' : The range in which resource configurations can be selected can be configured here.
                  The subkeys 'minimum' and 'maximum' accept normal kubernetes resource definitions (cpu and memory)
    """
    def __init__(self, enabled_mms, experiment_config, api, strategy_config) -> None:
        super().__init__()
        self.enabled_mms = enabled_mms
        self.experiment_config = experiment_config
        self.strategy_config = strategy_config
        self.api = api
        try:
            self.max_resources = {
                "cpu": parse_cpu(strategy_config["resources"]["maximum"]["cpu"]),
                "memory": parse_memory(strategy_config["resources"]["maximum"]["memory"])
            }

            self.min_resources = {
                "cpu": parse_cpu(strategy_config["resources"]["minimum"]["cpu"]),
                "memory": parse_memory(strategy_config["resources"]["minimum"]["memory"])
            }
        except KeyError as e:
            raise ValueError("Configuration for Strategy is incomplete", e)

    def run(self):
        raise NotImplementedError()
