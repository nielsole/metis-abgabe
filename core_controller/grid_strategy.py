import asyncio
import copy
import logging
import os
from concurrent.futures.thread import ThreadPoolExecutor
from time import sleep
from typing import List, Iterable

from core_controller.abstract_strategy import AbstractStrategy
from experiment_controller.experiment import Experiment
from utils.utils import resource_in_range


class GridStrategy(AbstractStrategy):
    """
    This strategy creates a grid of experiment datapoints.
    Along each resource dimension the experiments are spaced exponentially apart.
    Config options:
    'stepSize' : The multiplication factor for the new experiment resource
    'maxParallelism' : The maximum number of experiments that should be run in parallel.
    'optimizeContainer' : Defines which container of the pod should be optimized
    See AbstractStrategy for a list of options that all strategies support.
    """

    def __init__(self, enabled_mms, experiment_config, api, strategy_config) -> None:
        super().__init__(enabled_mms, experiment_config, api, strategy_config)
        try:
            self.step_factor = strategy_config["stepSize"]
        except KeyError as e:
            self.step_factor = 1.3
        try:
            if not isinstance(strategy_config["maxParallelism"], int):
                raise ValueError("GridStrategy parameter 'maxParallelism' must be integer")
            self.max_parallelism = strategy_config["maxParallelism"]
        except KeyError as e:
            default_parallelism = 5
            logging.info("Using default maximum paralellism of {}".format(default_parallelism))
            self.max_parallelism = default_parallelism

    def run(self) -> List[Experiment]:
        """
        Runs all experiments to completion. The resources configurations are picked in a grid pattern.
        :return: list of experiments with their results
        """
        tasks = list()
        resource_configs = list()
        loop = asyncio.get_event_loop()
        experiments = list(self.get_steps())
        try:
            semaphore = asyncio.Semaphore(self.max_parallelism)
            # The relatively high number of worker threads is due to some external calls still blocking.
            # Using a higher number of workers should reduce this problem
            executor = ThreadPoolExecutor(max_workers=5)
            loop.set_default_executor(executor)
            logging.info("Will run {} experiments".format(len(experiments)))
            for experiment in experiments:
                task: asyncio.Task
                task = asyncio.ensure_future(self.__run_experiment_and_cleanup(experiment, semaphore), loop=loop)
                tasks.append(task)
                resource_configs.append(experiment.resources)
            loop.run_until_complete(asyncio.gather(*tasks))
            logging.info("Done with running tasks in GridSearch")
            for i, experiment in enumerate(experiments):
                result = experiment.result
                logging.info("--------------------")
                if result.sla_met():
                    logging.info("SLAs were fulfilled")
                else:
                    logging.info("SLAs were not fulfilled")
                logging.info(result)
                logging.info(resource_configs[i])
        except Exception as e:
            print(e)
        finally:
            loop.stop()
            loop.close()
            loop = asyncio.new_event_loop()
            logging.warning("Cleaning up left-over experiments in ten seconds...")
            for i in range(10):
                print(10 - i)
                sleep(1)
            logging.info("Cleaning up left-over experiments")
            deletions = list()

            async def delete_experiment(experiment):
                await experiment.release_resources()
                logging.info("{}: {} objects deleted".format(experiment.pod_name, await experiment.release_resources()))

            for experiment in experiments:
                deletion: asyncio.Task
                deletion = asyncio.ensure_future(delete_experiment(experiment), loop=loop)
                deletions.append(deletion)
            loop.run_until_complete(asyncio.gather(*deletions))
            loop.close()
        return experiments

    def get_steps(self) -> Iterable:
        """
        Picks the resource configurations that should be run.
        :return: Generator of resource configurations
        """
        current_resources = self.min_resources
        previous_memory = self.min_resources["memory"]
        while resource_in_range(current_resources, self.min_resources, self.max_resources):
            while resource_in_range(current_resources, self.min_resources, self.max_resources):
                experiment = Experiment(self.experiment_config, current_resources, self.enabled_mms, self.api, self.strategy_config["optimizeContainer"])
                yield experiment
                current_resources = copy.deepcopy(current_resources)
                current_resources["cpu"] *= self.step_factor
            current_resources = copy.deepcopy(current_resources)
            current_resources["memory"] = previous_memory * self.step_factor
            current_resources["cpu"] = self.min_resources["cpu"]
            previous_memory = current_resources["memory"]

    async def __run_experiment_and_cleanup(self, experiment: Experiment, semaphore: asyncio.Semaphore):
        async with semaphore:
            await experiment.run()
            # Unfortunately necessary, as sometimes the services need a little extra time to set up networking esp. on GKE
            # Possibly replace by checking the number of endpoints on a service
            await asyncio.sleep(10)
        return await experiment.release_resources()
