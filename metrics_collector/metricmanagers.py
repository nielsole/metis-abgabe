from typing import Dict, Any

from experiment_controller.experiment_result import ExperimentResult


class MetricManager(object):

    def __init__(self, config: dict) -> None:
        super().__init__()
        self.config = config

    def evaluate(self, result: ExperimentResult) -> Dict[str, Dict[str, Any]]:
        pass
