from typing import Dict, Any

from experiment_controller.experiment_result import ExperimentResult
from metrics_collector.metricmanagers import MetricManager


class SiegeMetricManager(MetricManager):
    """
    Evaluates metrics created by the tool siege. Currently transaction rate, longest transaction and throughput are supported.
    """
    def evaluate(self, config, result: ExperimentResult) -> Dict[str, Dict[str, Any]]:
        slas = dict()
        container_name, logs = list(result.logs["test"].items())[0]
        for line in logs.split("\n"):
            if "Transaction rate" in line:
                value = line.split("\t")[1]
                value_parsed = float(value.strip().split(" ")[0])
                try:
                    slas["transaction_rate"] = {
                        "met": value_parsed > float(config["transactionRate"]),
                        "threshold": float(config["transactionRate"]),
                        "value": value_parsed
                    }
                except KeyError:
                    pass
            if "Longest transaction" in line:
                longest_transaction = line.split("\t")[1]
                longest_transaction_float = float(longest_transaction.strip())
                try:
                    slas["longest_transaction_time"] = {
                        "met": longest_transaction_float < float(config["longestTransaction"]),
                        "threshold": float(config["longestTransaction"]),
                        "value": longest_transaction_float
                    }
                except KeyError:
                    pass
            if "Throughput" in line:
                value = line.split("\t")[2]
                value_parsed = float(value.strip().split(" ")[0])
                try:
                    slas["throughput"] = {
                        "met": value_parsed < float(config["throughput"]),
                        "threshold": float(config["throughput"]),
                        "value": value_parsed
                    }
                except KeyError:
                    pass
        return slas
