import datetime

from pytimeparse import parse

from experiment_controller.experiment_result import ExperimentResult
from metrics_collector.metricmanagers import MetricManager


class InternalMetricManager(MetricManager):
    def evaluate(self, result: ExperimentResult):
        slas = dict()
        met = False
        max_execution_time = parse(self.config["maximumExecutionTime"])
        try:
            met = result.test_execution_time < datetime.timedelta(seconds=max_execution_time)
        except KeyError:
            pass
        slas["maximumExecutionTime"] = {
            "met": met,
            "threshold": max_execution_time,
            "value": result.test_execution_time.total_seconds()
        }
        return slas
