import json
import operator
from typing import Dict, Any

from jsonpath_ng import parse, logging

from experiment_controller.experiment_result import ExperimentResult
from metrics_collector.metricmanagers import MetricManager


class JSONOutputManager(MetricManager):
    def comp(self, value, chosen_operator, threshold):
        operators = {
            "eq": operator.eq,
            "gt": operator.gt,
            "lt": operator.lt,
            "le": operator.le,
            "ge": operator.ge
        }
        return operators[chosen_operator](value, threshold)

    def evaluate(self, result: ExperimentResult) -> Dict[str, Dict[str, Any]]:
        """
        Tries to interpret the test container output as JSON and evaluates the configured SLO
        :param result:
        :return:
        """
        slas = dict()
        container_name, logs = list(result.logs["test"].items())[0]
        try:
            j_obj = json.loads(logs)
        except json.decoder.JSONDecodeError as e:
            logging.debug("Could not decode logs as json: ", logs)
            raise e
        for metric in self.config["metrics"]:
            value = parse(metric["path"]).find(j_obj)[0].value
            slas[metric["path"]] = {
                "met": self.comp(value, metric["operator"], metric["threshold"]),
                "threshold": metric["threshold"],
                "value": value,
            }
        return slas
