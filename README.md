# Metis

Metis is a framework that supports application providers in determining the sizing of their containers in cloud environments.
Currently Metis supports Kubernetes and its resource requests for memory and CPU.

## Install

### Docker

```
docker build -t eu.gcr.io/kubernetes-berlin/metis-base . -f base.Dockerfile
docker build -t eu.gcr.io/kubernetes-berlin/metis .
```

### Ubuntu

First, set up a virtual environment with Python 3.6

```
pip install -r requirements.txt
```

## Usage

The following command-line arguments are supported:

```
usage: main.py [-h] [--kubeconfig [kube-config]] [metis-config]

Metis facilitates loadtesting for estimating resource requirements.

positional arguments:
  metis-config          the path to the metis configuration file

optional arguments:
  -h, --help            show this help message and exit
  --kubeconfig [kube-config]
                        the path to the kubectl configuration file
```

The results of the tests are written into the working directory to `artifacts`.

Graphs can be generated with `python graphs.py` which assumes the `artifacts` directory to be in the same repository.

### Using a Google Cloud Cluster

The used library does not support the same authentication mechanism as Google Cloud.
To work around this, a local `kubectl` proxy can be started and Metis pointed to it:

```
kubectl proxy &
./main.py --kubeconfig empty_kube_config.yml
```