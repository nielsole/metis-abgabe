#!/usr/bin/python3
import argparse
import logging

import pykube

from core_controller.strategies import available_strategies
from metrics_collector.json_output import JSONOutputManager
from utils.artifacts_creation import write_experiment_result_yaml, write_yaml_summary, create_html_page
from metrics_collector.internal import InternalMetricManager
from metrics_collector.siege import SiegeMetricManager
from core_controller.grid_strategy import GridStrategy
from utils.utils import parse_config


def setup(conf_path, kubeconfig_path):
    """
    sets up metric managers and initializes strategy with them.
    :param conf_path:
    :param kubeconfig_path:
    :return:
    """
    api = pykube.HTTPClient(pykube.KubeConfig.from_file(kubeconfig_path))
    config = parse_config(conf_path)
    try:
        pykube.Namespace.objects(api).get(name=config["namespace"])
    except pykube.exceptions.ObjectDoesNotExist:
        logging.error(
            "Attempting to run in namespace '{}', which does not exist. Exiting...".format(config["namespace"]))
        exit(1)
    available_mms = {'internal': InternalMetricManager,
                     'siege': SiegeMetricManager,
                     'jsonOutput': JSONOutputManager}
    enabled_mms = dict()
    for mms_name in config["evaluation"].keys():
        enabled_mms[mms_name] = available_mms[mms_name](config["evaluation"][mms_name])
    try:
        strategy = available_strategies[config["strategy"]["name"]](enabled_mms, config["experiment"], api,
                                                                    config["strategy"]["options"])
    except KeyError:
        logging.info("Using default strategy")
        strategy = GridStrategy(enabled_mms, config["strategy"], config["experiment"], api)
    return strategy


def evaluate_results(experiments):
    for experiment in experiments:
        write_experiment_result_yaml(experiment)
    write_yaml_summary(experiments)
    sla_names = set()
    for experiment in experiments:
        for key in experiment.result.slas.keys():
            sla_names.add(key)
    create_html_page(experiments)


def main(conf_path, kubeconfig_path):
    strategy = setup(conf_path, kubeconfig_path)
    experiments = strategy.run()
    evaluate_results(experiments)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Metis facilitates loadtesting for estimating resource requirements.')
    parser.add_argument('conf', metavar='metis-config', type=str, nargs='?', default='.metis.yml',
                        help='the path to the metis configuration file')
    parser.add_argument('--kubeconfig', metavar='kube-config', type=str, nargs='?', default='.kube/config.json',
                        help='the path to the kubectl configuration file')
    args = parser.parse_args()
    logging.getLogger().setLevel(logging.INFO)
    main(args.conf, args.kubeconfig)
