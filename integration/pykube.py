import copy
import datetime
import logging
import sys
from asyncio import sleep
from typing import Dict

import pykube
import yaml
from requests import HTTPError, exceptions


class PykubeKubernetesExperiment(object):
    """
    Integration into Kubernetes. contains abstraction on top of pykube.
    """
    def __init__(self, prefix: str, api: pykube.HTTPClient) -> None:
        super().__init__()
        self.prefix = prefix
        self.api = api
        self.used_resource_kinds = set()

    def __get_pod(self, pod_name) -> pykube.Pod:
        pod = pykube.Pod.objects(self.api).get(name=self.__prefixy(pod_name))
        return pod

    def is_done(self, pod_name) -> bool:
        pod = self.__get_pod(pod_name)
        return pod.obj["status"]["phase"] in ["Succeeded", "Failed", "Error", "OOMKilled"]

    def is_failed(self, pod_name) -> bool:
        """
        Has any of the containers in the pod terminate unsuccessfully?
        Raises exception if any container is still running
        :param pod_name:
        :return:
        """
        pod = self.__get_pod(pod_name)
        has_failed_container = False
        for containerStatus in pod.obj["status"]["containerStatuses"]:
            try:
                if containerStatus["state"]["terminated"]["reason"] in ["OOMKilled", "Failed", "Error"]:
                    has_failed_container = True
            except KeyError:
                logging.warning(
                    "Container in pod {} is not yet terminated: {}".format(pod_name, containerStatus["name"]))
        return has_failed_container

    async def collect_container_logs(self, pod, container_name):
        """
        Retrieves logs for the given container with exponential backoff
        :param pod:
        :param container_name:
        :return:
        """
        backoff = 1
        for i in range(3):
            try:
                return pod.logs(container_name)
            except HTTPError:
                logging.warning("Could not retrieve logs from container: {}({})".format(container_name,
                                                                                        pod.obj["metadata"]["name"]))
                await sleep(backoff)
                backoff *= 2

    async def collect_logs(self, pod_name: str) -> Dict:
        """
        Collects logs for all containers
        :param pod_name: without experiment prefix
        :return: dict[container name]logs
        """
        pod_logs = dict()
        pod = self.__get_pod(pod_name)
        for container in pod.obj["spec"]["containers"]:
            pod_logs[container["name"]] = await self.collect_container_logs(pod, container["name"])
        return pod_logs

    def is_oom(self, pod_name: str) -> bool:
        """
        For finished pods returns whether any of the containers were OOMKilled.
        :param pod_name:
        :return: is the pod finished and did any container oomdie?
        """
        if not self.is_done(pod_name):
            return False
        pod = self.__get_pod(pod_name)
        for container in pod.obj["status"]["containerStatuses"]:
            try:
                if container["state"]["terminated"]["reason"] == "OOMKilled":
                    return True
            except KeyError:
                pass
        return False

    def __prefixy(self, pod_name: str) -> str:
        return "{}-{}".format(self.prefix, pod_name)

    def get_execution_time(self, pod_name: str, container_name: str = None) -> datetime.timedelta:
        """
        Calculates delta between start and end time of finished container
        :param pod_name:
        :param container_name: optional
        :return:
        """
        if not self.is_done(pod_name):
            raise Exception("Cannot measure execution time of a running container")
        pod = self.__get_pod(pod_name)
        begin = pod.obj["status"]["startTime"]
        container_offset = 0
        if container_name is not None:
            for i, containerStatus in range(pod.obj["status"]["containerStatuses"]):
                if containerStatus["name"] == container_name:
                    container_offset = i
        containerStatus = pod.obj["status"]["containerStatuses"][container_offset]
        end = containerStatus["state"]["terminated"]["startedAt"]
        begin_parsed = datetime.datetime.strptime(begin, "%Y-%m-%dT%H:%M:%SZ")
        end_parsed = datetime.datetime.strptime(end, "%Y-%m-%dT%H:%M:%SZ")
        if containerStatus["state"]["terminated"]["reason"] not in ["Completed", "Failed", "Error"]:
            yaml.dump(pod.obj, sys.stdout)
            logging.getLogger().warning("Unexpected reason used for determining success of container: {}".format(
                containerStatus["state"]["terminated"]["reason"]))
            raise Exception("Cannot measure execution time of a running container")
        return end_parsed - begin_parsed

    def is_ready(self, pod):
        """
        Does Kubernetes consider the pod ready?
        :param pod:
        :return:
        """
        for condition in pod.obj["status"]["conditions"]:
            if condition["type"] != "Ready":
                continue
            if condition["status"] == "True":
                return True
            return False
        raise Exception("Cannot determine if pod is ready. Does not have 'Ready' condition: {}".format(pod.name))

    async def wait_til_started(self, pod_name: str):
        """
        Queries repeatedly until pod is reported as ready, or 5 connection timeouts have occured.
        :param pod_name:
        :return:
        """
        tries = 5
        while True:
            try:
                pod = self.__get_pod(pod_name)

                phase = pod.obj["status"]["phase"]
                if phase in ["Running"]:
                    if self.is_ready(pod):
                        logging.info("Pod is not yet ready: " + pod.name)
                        return
                elif phase == "Failed":
                    return True
                elif phase != "Pending":
                    raise Exception("Pod is in unexpected phase: {}({})".format(phase, pod.name))
            except exceptions.ConnectionError as e:
                if tries == 0:
                    raise e
                logging.warning("Could not reach API server")
                tries -= 1
            await sleep(5)

    def create(self, element):
        """
        Creates the given Kubernetes object and registers its kind for deletion
        :param element:
        :return:
        """
        try:
            Kind = getattr(pykube, element["kind"])
            self.used_resource_kinds.add(Kind)
            k8s_obj = Kind(self.api, self.__prefix_and_label_element(element))
            k8s_obj.create()
        except HTTPError as e:
            raise e
        except AttributeError:
            logging.getLogger().warning("Unsupported auxiliary kubernetes object of type: {}".format(element["kind"]))

    def __prefix_and_label_element(self, element):
        element = copy.deepcopy(element)
        try:
            if element["metadata"] is not None:
                pass
        except KeyError:
            element["metadata"] = {}

        element["metadata"]["name"] = self.__prefixy(element["metadata"]["name"])
        try:
            element["metadata"]["labels"].update({"metis-experiment": self.prefix})
        except KeyError:
            element["metadata"]["labels"] = {"metis-experiment": self.prefix}
        return element

    def delete(self):
        """
        Deletes all resources in K8s used by this experiment
        :return:
        """
        i = 0
        for Kind in self.used_resource_kinds:
            try:
                for kind in Kind.objects(self.api).filter(selector={"metis-experiment": self.prefix}) or list():
                    kind.delete()
                    i += 1
            except TypeError:
                pass
        return i
