import copy
import logging
import string
import random
from asyncio import sleep
from typing import Dict, Tuple, Iterable

import pykube
from pybars import Compiler

from experiment_controller.experiment_result import ExperimentResult
from metrics_collector.metricmanagers import MetricManager
from integration.pykube import PykubeKubernetesExperiment


def handle_element(value, replacement_map: dict, compiler=Compiler()):
    if type(value) is dict:
        return recursive_replace(value, replacement_map)
    if type(value) is list:
        return list(map(lambda element: handle_element(element, replacement_map, compiler), value))
    if type(value) is str:
        compiled_value = compiler.compile(value)
        new_value = compiled_value(replacement_map)
        return new_value
    return value


def recursive_replace(template: dict, replacement_map: dict):
    compiler = Compiler()
    for key, value in template.items():
        template[key] = handle_element(value, replacement_map, compiler)
    return template


class Experiment(object):
    """
    An experiment manages one resource configuration.
    It sets up all the objects in Kubernetes and manages their lifecycle.
    Experiments are prefixed to avoid naming collisions.
    """

    metric_managers = dict()  # type: Dict[str, MetricManager]
    done = False
    started = False

    def _generate_prefix(self, length=6):
        """
        Generates a random prefix of lowercase ascii characters and digits
        :param length: prefix length
        :return: prefix
        """
        return ''.join(
            [random.choice(string.ascii_lowercase)] + [random.choice(string.ascii_lowercase + string.digits) for _ in
                                                       range(length)])

    def __init__(self,
                 config: dict,
                 resources: dict,
                 metric_managers: Dict[str, MetricManager],
                 api: pykube.HTTPClient, optimizeContainer: str
                 ) -> None:
        super().__init__()
        self.metric_managers = metric_managers
        self.config = config
        self.test_subject_container_name = optimizeContainer
        self.job_template = copy.deepcopy(config["definition"]["template"])
        self.prefix = self._generate_prefix()
        self.pod_name = self.job_template["metadata"]["name"]
        self.config = config
        self.resources = resources
        self.api = api
        self.result = ExperimentResult()
        self.kubernetes_experiment = PykubeKubernetesExperiment(self.prefix, api)
        for container in self.job_template["spec"]["containers"]:
            if container["name"] == self.test_subject_container_name:
                container["resources"] = {"requests": resources,
                                          "limits": resources}

    def getAuxiliaryPods(self) -> Iterable[Tuple[str, pykube.Pod]]:
        for name, element in self.context["auxiliary"].items():
            if element["kind"] == "Pod":
                yield name, element["metadata"]["name"]

    async def _execute_test(self):
        logging.info("Setting up test subject")
        self.kubernetes_experiment.create(recursive_replace(self.job_template, self.context))
        self.started = True

        logging.info("Waiting for test subject to come up")
        await self.kubernetes_experiment.wait_til_started(self.pod_name)
        await sleep(15)
        testing_pod_name = await self.__initialize_testing()
        while True:
            testing_done = self.kubernetes_experiment.is_done(testing_pod_name)
            subject_done = self.kubernetes_experiment.is_done(self.pod_name)
            if testing_done or subject_done:
                logging.info("The following pods are done: {}(Testing), {}(Subject)".format(testing_done, subject_done))
                # Sometimes inconsistent self.results from API.
                # Sometimes Testing pod is still pending when the experiment is already done, but no oom is detected
                await sleep(2)
                break
            logging.debug("Sleeping, while experiment is underway")
            await sleep(5)
        return testing_pod_name

    async def run(self):
        """
        Manages the lifecycle of the experiment and triggers creation of all necessary resources in the right order.
        After the experiment is over, metrics are collected.
        :return:
        """
        if self.started:
            raise Exception("You cannot start a job twice")
        if self.done:
            raise Exception("Job already finished")
        self.context = {"prefix": self.prefix}
        logging.info("Setting up auxiliary services")
        await self.__initialize_auxiliary_services()
        testing_pod_name = await self._execute_test()

        if self.kubernetes_experiment.is_oom(self.pod_name):
            logging.info("OOM was detected")
        else:
            logging.info("No OOM was detected")
        self.result.oom = self.kubernetes_experiment.is_oom(self.pod_name)
        # Collect logs
        self.result.logs["auxiliary"] = dict()
        for pod_name, element in self.getAuxiliaryPods():
            self.result.logs["auxiliary"][pod_name] = await self.kubernetes_experiment.collect_logs(pod_name)
        testing_logs = await self.kubernetes_experiment.collect_logs(testing_pod_name)
        self.result.logs["test"] = testing_logs
        # TODO Add logging aggregation for auxiliary services
        if (not self.kubernetes_experiment.is_oom(self.pod_name)) and self.kubernetes_experiment.is_failed(self.pod_name):
            self.result.failed = True
            logging.info("Failure of pod was detected: {}".format(self.pod_name))
            return
        try:
            self.__collect_metrics()
        except Exception as e:
            logging.exception(e)
            self.result.slas["error_free"] = False
        return

    def sla_met(self) -> bool:
        if not self.done:
            raise Exception("Metrics will not be available before the start of the operation")
        return self.result.sla_met()

    def __collect_metrics(self):
        try:
            self.result.test_execution_time = self.kubernetes_experiment.get_execution_time(
                self.context["test"]["metadata"]["name"])
        except Exception as e:
            logging.error(e)
        sla = dict()
        for name, metric_manager in self.metric_managers.items():
            print(name)
            try:
                metric_manager_meets_sla = metric_manager.evaluate(self.result)
                sla.update(metric_manager_meets_sla)
            except Exception as e:
                logging.error("Metric Manager ({}) had an error. Skipping...".format(name))
                logging.exception(e)
        self.result.slas = sla

    async def __initialize_auxiliary_services(self):
        auxiliary_template = copy.deepcopy(self.config["auxiliary"])
        auxiliary_template = handle_element(auxiliary_template, self.context)
        auxiliary_objects = {}
        for element in auxiliary_template:
            auxiliary_objects[element["metadata"]["name"]] = element
            self.kubernetes_experiment.create(element)
        self.context["auxiliary"] = auxiliary_objects
        return self.context

    async def __initialize_testing(self):
        element = copy.deepcopy(self.config["test"]["template"])
        element = recursive_replace(element, self.context)
        self.context["test"] = element
        self.kubernetes_experiment.create(element)
        return element["metadata"]["name"]

    async def release_resources(self):
        """
        Releases all resources allocated by this experiment in the COT
        :return:
        """
        return self.kubernetes_experiment.delete()
