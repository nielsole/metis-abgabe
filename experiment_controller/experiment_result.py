

class ExperimentResult(object):
    """
    Datastructure containing all information about the results of an experiment.
    """
    def __init__(self) -> None:
        super().__init__()
        self.oom = False
        self.failed = False
        self.slas = dict()
        self.logs = dict()
        self.test_execution_time = None

    def sla_met(self) -> bool:
        """
        :return: are all sla met?
        """
        # if the pod OOMed there are no SLAs
        if len(self.slas) == 0:
            return False
        for name, value in self.slas.items():
            if not value["met"]:
                return False
        return True

    def __str__(self) -> str:
        str_representation = ""
        for sla_name, sla_met in self.slas.items():
            str_representation += "{}\t{}\n".format(sla_name, sla_met)
        return str_representation
