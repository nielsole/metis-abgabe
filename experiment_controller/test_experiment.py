import unittest

from experiment_controller.experiment import recursive_replace, Experiment


class RecursiveReplaceTestCase(unittest.TestCase):
    def test_empty(self):
        self.assertEqual(dict(), recursive_replace({}, {}))
        self.assertEqual({"key": "value"}, recursive_replace({"key": "value"}, {}))

    def test_primitives(self):
        self.assertEqual(5, recursive_replace({"number": 5}, {"garbage": "something"})["number"])
        self.assertEqual("5", recursive_replace({"number": "{{ garbage }}"}, {"garbage": 5})["number"])
        self.assertEqual("5", recursive_replace({"number": "{{ garbage }}"}, {"garbage": "5"})["number"])
        # self.assertEqual("5 ",recursive_replace({"number": "{{ garbage }} "}, {"garbage": "5"})["number"])
        self.assertEqual("5f", recursive_replace({"number": "{{ garbage }}"}, {"garbage": "5f"})["number"])
        self.assertEqual(["Hallo", "hier"],
                         recursive_replace({"number": ["{{ garbage }}", "hier"]}, {"garbage": "Hallo"})["number"])

    def test_nesting(self):
        self.assertEqual({"root": {"child": {"grandchild": "Hallo"}}},
                         recursive_replace({"root": {"child": {"grandchild": "{{ garbage }}"}}}, {"garbage": "Hallo"}))


class ExperimentTestCase(unittest.TestCase):
    def test_generate_prefix(self):
        for i in range(100):
            prefix = Experiment._generate_prefix(None)
            self.assertTrue(prefix[0].isalpha(), "Prefix is not valid dns name")


if __name__ == '__main__':
    unittest.main()
